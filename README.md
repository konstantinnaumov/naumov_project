# naumov_project
## Task  1:
Describe the configuration of Nginx, where it will act as a balancer using upstream.
## Task 2:
Build a docker image that includes the Nginx application and web server, open port 80 at startup and add the container to the system autostart.
Comment to the task:
you can use a static web page instead of an application for this task.
## Task 12:
Raise a Kubernetes cluster of three nodes. Make a primary configuration and deploy a container into it.
